# Lyka Notification Gateway
A project to facilitate notification sending (push/email/sms) from a single gateway endpoint.

## API Specification
[Swagger specification file](openapi.yaml)

#### Required dependencies
```
requests
pynamodb
vonage
```

1. Install and set-up AWS CLI and Serverless Framework
2. Create a Python Virtual Environment
```
# Install pyenv

# Install desired Python version using pyenv
pyenv install 3.8.6

# Create a venv with pyenv
pyenv virtualenv 3.8.6 python3.8.6

Activate env on project directory
pyenv local python3.8.6
pyenv activate
```

3. Install the required dependencies to the exact directory, this is used by Serverless to create a lambda layer
```
pip install -r requirements.txt -t ./lib/python
```
The **./lib** path will be referenced in the [serverless.yaml](serverless.yml) file. Serverless will zip it and upload it as a layer. The layer is then referenced by the lambda functions.

How to set-up Vonage API layer individually (please ignore this)

```
cd layer/voyage/python   
pip install vonage -t .   
cd .. 
zip -r vonage_layer.zip .  
```
Do the same for PynamoDB too, the layers source zip files are then referenced in the [serverless.yaml](serverless.yml)


#### Deployment
```
serverless deploy --region ap-southeast-1 --aws-profile default
```

#### Architecture
To be updated
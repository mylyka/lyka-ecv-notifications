import json
import os
from pynamodb.models import Model
from pynamodb.indexes import LocalSecondaryIndex, AllProjection
from pynamodb.attributes import ListAttribute, UnicodeAttribute, UTCDateTimeAttribute, MapAttribute, BooleanAttribute, NumberAttribute
from datetime import datetime
from urllib.parse import quote_plus

AWS_REGION = 'ap-southeast-1'

class BaseModel(Model):
    def to_json(self, indent=2):
        return json.dumps(self.to_dict(), indent=indent)
        
    def to_dict(self):
        ret_dict = {}
        for name, attr in self.attribute_values.items():
            ret_dict[name] = self._attr2obj(attr)
        
        return ret_dict

    def _attr2obj(self, attr):
        # compare with list class. It is not ListAttribute.
        if isinstance(attr, list):
            _list = []
            for l in attr:
                _list.append(self._attr2obj(l))
            return _list
        elif isinstance(attr, MapAttribute):
            _dict = {}
            for k, v in attr.attribute_values.items():
                _dict[k] = self._attr2obj(v)
            return _dict
        elif isinstance(attr, datetime):
            return attr.isoformat()
        else:
            return attr

class NotificationModel(BaseModel):
    class Meta:
        table_name = os.environ['NOTIF_REQ_TABLE_NAME']
        region = AWS_REGION
    uid = UnicodeAttribute(hash_key=True)
    users = ListAttribute()
    channels = ListAttribute()
    subject = UnicodeAttribute()
    message = UnicodeAttribute()
    html_message = UnicodeAttribute(null=True)
    payload = MapAttribute(null=True)
    push_successful = NumberAttribute(null=True)
    push_failed = NumberAttribute(null=True)
    email_successful = NumberAttribute(null=True)
    email_failed = NumberAttribute(null=True)
    sms_successful = NumberAttribute(null=True)
    sms_failed = NumberAttribute(null=True)
    datetime = UTCDateTimeAttribute(null=True)

class NotificationChannelViewModel(LocalSecondaryIndex):
    # LSI for notification detail table
    class Meta:
        projection = AllProjection
    user_id = UnicodeAttribute(hash_key=True)
    channel_sort = UnicodeAttribute(range_key=True)

class NotificationViewModel(LocalSecondaryIndex):
    class Meta:
        projection = AllProjection
    user_id = UnicodeAttribute(hash_key=True)
    date_time = UTCDateTimeAttribute(range_key=True)

class NotificationDetailModel(BaseModel):
    class Meta:
        table_name = os.environ['NOTIF_DETAIL_TABLE_NAME']
        region = AWS_REGION
    user_id = UnicodeAttribute(hash_key=True)
    notification_id = UnicodeAttribute(range_key=True)

    # LSI for querying specific channel
    channel_index = NotificationChannelViewModel() 
    channel_sort = UnicodeAttribute()
    # LSI for querying all channel
    date_index = NotificationViewModel()
    date_time = UTCDateTimeAttribute()

    notification_channel = UnicodeAttribute()
    request_id = UnicodeAttribute()
    has_been_read = BooleanAttribute()
    

class NotificationPreferenceModel(BaseModel):
    class Meta:
        table_name = 'notification-preference'
        region = AWS_REGION
    user_id = UnicodeAttribute(hash_key=True)
    allow_sms = BooleanAttribute()
    allow_email = BooleanAttribute()
    allow_push = BooleanAttribute()

class PushNotificationTokenModel(BaseModel):
    class Meta:
        table_name = os.environ['TOKEN_TABLE_NAME']
        region = AWS_REGION
    userId = UnicodeAttribute(hash_key=True)
    deviceToken = UnicodeAttribute(range_key=True)
    deviceType = UnicodeAttribute()
        

def create_notification_request(notification):

    new_notification = NotificationModel(
        notification['uuid'],
        users = notification['users'],
        channels = notification['channels'],
        subject = notification['subject'],
        message = notification['message'],
        payload = notification['payload'],
        html_message = notification['html_message'],
        datetime = notification['datetime']
    )

    new_notification.save()

def get_notification_request(notification_id):
    notification = NotificationModel.get(notification_id)
    return notification.to_dict()


def update_notification_request(notification_id, channel, success_count, failed_count):
    notification_request = NotificationModel.get(notification_id)

    if(channel == 'PUSH'):
        notification_request.update(
            actions=[
                NotificationModel.push_successful.set(success_count),
                NotificationModel.push_failed.set(failed_count)
            ]
        )
    elif(channel == 'EMAIL'):
        notification_request.update(
            actions=[
                NotificationModel.email_successful.set(success_count),
                NotificationModel.email_failed.set(failed_count)
            ]
        )
    elif(channel == 'SMS'):
        notification_request.update(
            actions=[
                NotificationModel.sms_successful.set(success_count),
                NotificationModel.sms_failed.set(failed_count)
            ]
        )

def create_notification_detail(notification_details):
    with NotificationDetailModel.batch_write() as batch:
        new_notification_details = [NotificationDetailModel(
            user_id = detail['user_id'],
            notification_id = detail['notification_id'],
            channel_sort = detail['channel_sort'],
            notification_channel = detail['notification_channel'],
            request_id = detail['request_id'],
            has_been_read = detail['has_been_read'],
            date_time = detail['date_time']
        ) for detail in notification_details]
        
        for item in new_notification_details:
            batch.save(item)


def create_notification_token(token):
    new_token = PushNotificationTokenModel(
        userId = token['userId'],
        deviceType = token['deviceType'],
        deviceToken = token['deviceToken']
    )
    return new_token.save()

def delete_notification_token(token):
    token = PushNotificationTokenModel(
        userId = token['userId'],
        deviceToken = token['deviceToken']
    )

    return token.delete()

def get_device_token(user_id):
    try:
        tokens = PushNotificationTokenModel.query(user_id)
        return [token.to_dict() for token in tokens]
    except Model.DoesNotExist:
        print(f"[ERROR] Device token not found for user with user ID {user_id}")
        return None

def get_user_notifications(user_id, channel, page_size, last_evaluated_key=None):

    if(channel):
        notifications = NotificationDetailModel.channel_index.query(
            user_id, 
            NotificationDetailModel.channel_sort.startswith(channel),
            #attributes_to_get=["user_id", "notification_id", "notification_channel", "date_time", "has_been_read", "request_id"],
            limit=page_size,
            last_evaluated_key=last_evaluated_key,
            scan_index_forward=False)
    else:
        notifications = NotificationDetailModel.date_index.query(
            user_id,
            #attributes_to_get=["user_id", "notification_id", "notification_channel", "date_time", "has_been_read", "request_id"],
            limit=page_size,
            last_evaluated_key=last_evaluated_key,
            scan_index_forward=False)

    list_of_notification = [notification.to_dict() for notification in notifications]
    
    return {
        'notifications': list_of_notification,
        'last_evaluated_key': quote_plus(json.dumps(notifications.last_evaluated_key, separators=(',', ':')))
    }

def mark_as_read(user_id, notification_id):
    try:
        notification = NotificationDetailModel.get(user_id, notification_id)
        notification.update(
            actions=[
                NotificationDetailModel.has_been_read.set(True)
            ]
        )
    except Model.DoesNotExist:
        print(f"[ERROR] Notification with id {user_id}_{notification_id} does not exist.")


# TODO: implement proper set and get for user preference
def get_user_preference(userid):
    preference = {
        "email": True,
        "sms": True,
        "push": True
    }
    return preference
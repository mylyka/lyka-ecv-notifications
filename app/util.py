import requests

def get_user_contact(user_id):
    try:
        contact = requests.get('http://social.lyka.local/api/v3/profiles/GetUserContactInformation', {'userId': user_id})
        return contact.json()
    except requests.exceptions.ConnectionError as e:
        # TODO: raise exception to stop execution
        print("ERROR cannot connect to user contact information endpoint.", e)
        return None
    
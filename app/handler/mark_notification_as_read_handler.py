import json
from app.db.notification_db import mark_as_read
def lambda_handler(event, context):

    try:
        request_body = json.loads(event['body'])

        for notification in request_body['notifications']:
            mark_as_read(str(notification['user_id']), notification['notification_id'])

        return{
            'statusCode': 200
        }

    except KeyError as e:
        print('ERROR missing required field ', e)
        return{
            'statusCode': 400,
            'body': json.dumps({'message': 'Invalid payload/missing required field.'})
        }

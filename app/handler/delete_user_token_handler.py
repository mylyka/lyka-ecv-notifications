import os
import boto3
import json
import traceback
from app.db.notification_db import delete_notification_token

def lambda_handler(event, context):
    request_body = json.loads(event['body'])
    
    try:
        token = {
            'userId': str(request_body['userId']),
            'deviceType': request_body['deviceType'],
            'deviceToken': request_body['deviceToken']
        }
        response = delete_notification_token(token)

        print(response)

        return{
            'statusCode': 200,
            'body': json.dumps({"message":"Removed user device token."})
        }
    
    except KeyError as e:
        print("ERROR missing or invalid field: ", e)
        return{
            'statusCode': 400,
            'body': json.dumps({"message":"Error. Missing or invalid required field"})
        }

    except Exception as e:
        print(e)
        traceback.print_exc()
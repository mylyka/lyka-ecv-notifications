import os
import boto3
import uuid
import json
from datetime import datetime
from app.db.notification_db import create_notification_request

def lambda_handler(event, context):

    try:

        request_body = json.loads(event['body'])

        print(request_body)

        notification_id = str(uuid.uuid4())

        notification_detail = {
            'uuid': notification_id,
            'users': list(set(request_body['userID'])),
            'channels': request_body['channels'],
            'subject': request_body['subject'],
            'message': request_body['message'],
            'html_message': request_body['html_message'] if 'html_message' in request_body else None,
            'payload': request_body['payload'] if 'payload' in request_body else None,
            'datetime': datetime.now()
        }

        print(notification_detail)

        # Create notification record on DynamoDB
        create_notification_request(notification_detail)

        # Send notification request to SQS
        for channel in request_body['channels']:
            send_to_sqs(channel, notification_id)

        return{
            'statusCode': 200,
            'body': json.dumps({
                "message":"Request created",
                "notification_id": notification_id})
        }
    except KeyError as e:
        print('ERROR missing or invalid field: ', e)
        return{
            'statusCode': 400,
            'body': json.dumps({"message":"Error. Bad request body."})
        }

def send_to_sqs(type, notification_id):

    #sqs_client = boto3.client('sqs')
    sqs = boto3.Session()
    sqs_client = sqs.client(
        service_name = 'sqs',
        endpoint_url = os.environ["SQS_VPC_INTERFACE_ENDPOINT"]
    )

    queue_url = {
        'push': os.environ['PUSH_NOTIF_QUEUE_URL'],
        'email': os.environ['EMAIL_NOTIF_QUEUE_URL'],
        'sms':os.environ['SMS_NOTIF_QUEUE_URL']
    }

    response = sqs_client.send_message(
        QueueUrl=queue_url[type],
        MessageAttributes={
            'notificationId':{
                'DataType': 'String',
                'StringValue': notification_id
            }
        },
        MessageBody = notification_id

    )

    return response
import os
import boto3
import json
import traceback
from app.db.notification_db import create_notification_token

def lambda_handler(event, context):
    request_body = json.loads(event['body'])

    ALLOWED_DEVICE_TYPE = ["APNS", "GCM", "FCM"]
    
    try:
        if request_body['deviceType'] not in ALLOWED_DEVICE_TYPE:
            raise ValueError("Device type is not allowed.")
        
        token = {
            'userId': str(request_body['userId']),
            'deviceType': request_body['deviceType'],
            'deviceToken': request_body['deviceToken']
        }
        response = create_notification_token(token)

        print(response)

        return{
            'statusCode': 200,
            'body': json.dumps({"message":"Updated user device token."})
        }
    
    except KeyError as e:
        print("ERROR missing or invalid field: ", e)
        return{
            'statusCode': 400,
            'body': json.dumps({"message":"Error. Missing or invalid required field"})
        }
    except ValueError as e:
        print(e)
        return{
            'statusCode': 400,
            'body': json.dumps({"message":"Error. Invalid device type."})
        }

    except Exception as e:
        print(e)
        traceback.print_exc()
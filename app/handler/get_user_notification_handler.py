import json
from urllib.parse import unquote_plus
from app.db.notification_db import get_user_notifications

def lambda_handler(event, context):

    try:
        user_id = str(event['queryStringParameters']['userid'])
        channel_type = event['queryStringParameters']['channel'] if 'channel' in event['queryStringParameters'] else None
        page_size = int(event['queryStringParameters']['page_size']) if 'page_size' in event['queryStringParameters'] else 10
        last_evaluated_key = json.loads(unquote_plus(event['queryStringParameters']['last_evaluated_key'])) if 'last_evaluated_key' in event['queryStringParameters'] else None
        result = get_user_notifications(user_id, channel_type, page_size, last_evaluated_key)

        return{
            'statusCode': 200,
            'body': json.dumps(result)
        }

    except KeyError as e:
        print("ERROR missing required field", e)
        return{
            'statusCode': 400,
            'body': json.dumps({'message': 'Invalid payload/missing required field.'})
        }
    except TypeError:
        print("ERROR ", e)
        return{
            'statusCode': 400,
            'body': json.dumps({'message': 'Bad payload'})
        }



import boto3
import os
import json
from datetime import datetime
from app.db.notification_db import get_notification_request, update_notification_request, get_device_token, create_notification_detail, get_user_preference
from botocore.exceptions import ClientError

AWS_REGION = os.environ['PINPOINT_REGION']
PINPOINT_APP_ID = os.environ['PINPOINT_APP_ID']

def lambda_handler(event, context):
    print(event)

    for record in event['Records']:

        notification_id = record['messageAttributes']['notificationId']['stringValue']
        print("notificationid:", notification_id)

        notification_request = get_notification_request(notification_id)
        notification_type = notification_request['notification_type'] if 'notification_type' in notification_request else None
        #notification > {'channels': ['sms', 'email'], 'datetime': '2021-02-09T09:47:34.897276+00:00', 'message': 'testmessage.', 'subject': 'TestSubject', 'uid': 'c7ad15c6-6b7a-416e-89b9-9725d07539dc', 'users': [1234, 2345, 3345]}
        
        #Notification configuration
        action = 'OPEN_APP' #OPEN_APP/DEEP_LINK/URL
        title = notification_request['subject']
        message = notification_request['message']
        priority = 'normal'
        ttl = 30
        silent = False
        url = ""
        payload = notification_request['payload'] if 'payload' in notification_request else None

        successful_request = 0
        failed_request = 0
        sent_notificationd_details = []

        for user_id in notification_request['users']:

            user_preference = get_user_preference(user_id)
            # skip if user preference exist and there is an explicit no
            if ('push' in user_preference and not user_preference['push']):
                continue

            deviceTokens = get_device_token(str(user_id))

            # skip if no token found
            if(not deviceTokens):
                failed_request += 1
                continue

            for token in deviceTokens:
                deviceToken = token['deviceToken']
                serviceType = token['deviceType']

                response = send_message(create_message_request(deviceToken, serviceType, action, title, message, priority, silent, ttl, payload, url))
                request_id = response['ResponseMetadata']['RequestId']
                delivery_result = response['MessageResponse']['Result'][deviceToken]

                if(delivery_result['DeliveryStatus'] == 'SUCCESSFUL'):
                    date_time = datetime.now()
                    successful_request += 1
                    notification_detail = {
                    'user_id': str(user_id),
                    'channel_sort': 'push:' + str(date_time),
                    'notification_id': 'push:' + notification_id,
                    'notification_channel': 'push',
                    'notification_type': notification_type,
                    'request_id': request_id,
                    'has_been_read': False,
                    'date_time': date_time
                    }
                    sent_notificationd_details.append(notification_detail)
                else:
                    if(delivery_result['StatusCode'] == 410):
                        pass
                        # TODO: delete the token

                    failed_request += 1
                    print('ERROR', response['MessageResponse']['Result'])

        # batch create
        create_notification_detail(sent_notificationd_details)
        # update number of successful/failed request
        update_notification_request(notification_id, 'PUSH', successful_request, failed_request)
            

def create_message_request(token, service, action, title, message, priority, silent, ttl, payload, url):
    if service == "FCM" or service == "GCM":
            message_request = {
                'Addresses': {
                    token: {
                        'ChannelType': 'GCM'
                    }
                },
                'MessageConfiguration': {
                    'GCMMessage': {
                        'Action': action,
                        'Body': message,
                        'Priority': priority,
                        'SilentPush': silent,
                        'Title': title,
                        'TimeToLive': ttl,
                        'Url': url
                    }
                }
            }
            if(payload):
                message_request['MessageConfiguration']['GCMMessage'].setdefault('Data', payload)
    elif service == "APNS":
        message_request = {
            'Addresses': {
                token: {
                    'ChannelType': 'APNS'
                }
            },
            'MessageConfiguration': {
                'APNSMessage': {
                    'Action': action,
                    'Body': message,
                    'Priority' : priority,
                    'SilentPush': silent,
                    'Title': title,
                    'TimeToLive': ttl,
                    'Url': url
                }
            }
        }
        if(payload):
            message_request['MessageConfiguration']['APNSMessage'].setdefault('Data', payload)
    else:
        message_request = None

    return message_request

def send_message(message):

    client = boto3.client('pinpoint',region_name=AWS_REGION)
    application_id = PINPOINT_APP_ID
    try:
        response = client.send_messages(
            ApplicationId=application_id,
            MessageRequest=message
        )
    except ClientError as e:
        print(e.response['Error']['Message'])
    else:
        return response
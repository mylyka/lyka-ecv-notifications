import os
import vonage
from app.db.notification_db import get_notification_request, update_notification_request, create_notification_detail, get_user_preference
from datetime import datetime
from app.util import get_user_contact

client = vonage.Client(key=os.environ['VONAGE_KEY'], secret=os.environ['VONAGE_SECRET'])
sms = vonage.Sms(client)

def lambda_handler(event, context):
    
    for record in event['Records']:
        notification_id = record['messageAttributes']['notificationId']['stringValue']
        notification_request = get_notification_request(notification_id)
        notification_type = notification_request['notification_type'] if 'notification_type' in notification_request else None

        message = notification_request['message']
        
        successful_request = 0
        failed_request = 0
        sent_sms_details = []

        for user_id in notification_request['users']:

            user_preference = get_user_preference(user_id)
            # skip if user preference exist and there is an explicit no
            if ('sms' in user_preference and not user_preference['sms']):
                continue

            mobile_number = get_mobile_number(user_id)
            # skip if no phone number configured
            if(not mobile_number):
                continue

            response = send_sms(mobile_number, message)

            if(response["messages"][0]["status"] == "0"):
                date_time = datetime.now()
                successful_request += 1
                sent_sms = {
                    'user_id': str(user_id),
                    'channel_sort': 'sms:' + str(date_time),
                    'notification_id': 'sms:' + notification_id,
                    'notification_channel': 'sms',
                    'notification_type': notification_type,
                    'request_id': context.aws_request_id,
                    'has_been_read': False,
                    'date_time': date_time
                }
                sent_sms_details.append(sent_sms)
            else:
                failed_request += 1
                print(f"Message failed with error: {response['messages'][0]['error-text']}")

        create_notification_detail(sent_sms_details)
        update_notification_request(notification_id, 'SMS', successful_request, failed_request)


def get_mobile_number(user_id):
    try:
        contact = get_user_contact(user_id)
        if 'PhoneNumber' in contact['Data'] and contact['Data']['PhoneNumber'] is not None:
            return contact['Data']['PhoneNumber'] 
        else:
            return None
    except Exception as e:
        print("Error retrieving phone number.", e)
        return None

def send_sms(mobile_number, message):
    responseData = sms.send_message(
        {
            "from": "LYKA PLATFORM",
            "to": mobile_number,
            "text": message,
        }
    )
    return responseData

import boto3
import os
import requests
from app.db.notification_db import get_notification_request, create_notification_detail, update_notification_request, get_user_preference
from botocore.exceptions import ClientError
from datetime import datetime
from app.util import get_user_contact

AWS_REGION = os.environ['PINPOINT_REGION']
PINPOINT_APP_ID = os.environ['PINPOINT_APP_ID']
SENDER = os.environ['SENDER_EMAIL']
CHARSET = "UTF-8"

def lambda_handler(event, context):
    print(event)
    for record in event['Records']:
        notification_id = record['messageAttributes']['notificationId']['stringValue']
        print("notificationid:", notification_id)

        notification_request = get_notification_request(notification_id)

        # Email message configuration
        subject = notification_request['subject']
        message = notification_request['message']
        html_message = notification_request['html_message']

        successful_request = 0
        failed_request = 0
        sent_email_details = []

        for user_id in notification_request['users']:
            
            user_preference = get_user_preference(user_id)
            # skip if user preference exist and there is an explicit no
            if ('email' in user_preference and not user_preference['email']):
                continue

            to_email = get_user_email(user_id)
            # skip if no email configured
            if(not to_email):
                continue

            response = send_email(to_email, subject, message, html_message)
            request_id = response['ResponseMetadata']['RequestId']
            delivery_status = response['MessageResponse']['Result'][to_email]['DeliveryStatus']

            if(delivery_status == 'SUCCESSFUL'):
                date_time = datetime.now()
                successful_request += 1
                email_detail = {
                    'user_id': str(user_id),
                    'channel_sort': 'email:' + str(date_time),
                    'notification_id': 'email:' + notification_id,
                    'notification_channel': 'email',
                    'request_id': request_id,
                    'has_been_read': False,
                    'date_time': date_time
                    }
                sent_email_details.append(email_detail)
            else:
                failed_request += 1
                print('ERROR', response['MessageResponse']['Result'])

        create_notification_detail(sent_email_details)
        update_notification_request(notification_id, 'EMAIL', successful_request, failed_request)


def send_email(address, subject, message, html_message):
    client = boto3.client('pinpoint',region_name=AWS_REGION)
    try:
        response = client.send_messages(
            ApplicationId=PINPOINT_APP_ID,
            MessageRequest={
                'Addresses': {
                    address: {
                        'ChannelType': 'EMAIL'
                    }
                },
                'MessageConfiguration': {
                    'EmailMessage': {
                        'FromAddress': SENDER,
                        'SimpleEmail': {
                            'Subject': {
                                'Charset': CHARSET,
                                'Data': subject
                            },
                            'HtmlPart': {
                                'Charset': CHARSET,
                                'Data': html_message
                            },
                            'TextPart': {
                                'Charset': CHARSET,
                                'Data': message
                            }
                        }
                    }
                }
            }
        )
    except ClientError as e:
        print(e.response['Error']['Message'])
    else:
        return response


def get_user_email(user_id):
    
    try:
        contact = get_user_contact(user_id)
        if 'EmailAddress' in contact['Data'] and contact['Data']['EmailAddress'] is not None:
            return contact['Data']['EmailAddress'] 
        else:
            return None
    except:
        print("Error retrieving email.")
        return None